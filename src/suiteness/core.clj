(ns suiteness.core
  (:require [clojure.xml :as xml]
		    [clojure.data.csv :as csv]
           	[clojure.java.io :as io])))

; parse xml into edn and extract content:

(def a
  (-> "provider-a.xml"
          io/resource
          io/file
          xml/parse
          :content))

; grab all property codes:

(defn pc [m]
  (map #(:Code (:attrs %)) m))

; grab all rate plans from properties:

(defn rps [m]
  (map #(:content (first (:content %))) m))

; grab all codes from rate plans (map to rps):

(defn rpmap [m]
  (map #(:Code (:attrs %)) m))

; map property codes to rate codes:
; (zipmap (pc a) (map rpmap (rps a)))

; grab room type codes (map to rps)

(defn rt [m]
  (map #(:Code (:attrs (first (:content (first %))))) m))

(defn description [m]
  (map #(:RoomDescription (:attrs (first (:content (first %))))) m))

(defn currency [m]
  (map #(:NativeCurrency (:attrs (first (:content (first %))))) m))

(defn bt [m]
  (map #(:TotalRate (:attrs (first (:content (first %))))) m))

(defn at [m]
  (map #(:TotalRateInclusive (:attrs (first (:content (first %))))) m))

(defn av-rate [m]
  (map #(:BookableRate (:attrs (first (:content (first %))))) m))